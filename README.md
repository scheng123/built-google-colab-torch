torch.zip DEPRECATED, Google replaced K80 GPUs with T4 GPUs, requires CUDA 8 and gcc version below 5 (4.8 works) https://glcdn.githack.com/scheng123/built-google-colab-torch/raw/master/torch.zip
New torch with T4 GPU support, and most importantly, compiled with CUDA 10 and no gcc requirement! Also bundled with hdf5: https://gitlab.com/scheng123/built-google-colab-torch/raw/master/torch-t4.zip

Bundeled with hdf5 torch-full.zip, deprecated, see above: https://gitlab.com/scheng123/built-google-colab-torch/raw/master/torch-full.zip

hdf5: https://glcdn.githack.com/scheng123/built-google-colab-torch/raw/master/hdf5-1.8.12-linux-x86_64-shared.tar.gz